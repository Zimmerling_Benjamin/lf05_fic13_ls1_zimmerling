import java.util.Scanner;

/*
*  AUFGABENSTELLUNG
*  Geben Sie in der Konsole die nat�rlichen Zahlen von 1 bis n heraufz�hlend (bzw. von n bis 1
*  herunterz�hlend) aus. Erm�glichen Sie es dem Benutzer die Zahl n festzulegen. Nutzen Sie
*  zur Umsetzung eine for-Schleife.
*  a) 1, 2, 3, �, n for-Schleife
*  b) n, �, 3, 2, 1 for-Schleife
*/

public class ForSchleife {

	//MAIN-Methode
	public static void main(String[] args) {
	Scanner eingabe = new Scanner(System.in);
	System.out.println("AUFGABE: FOR-SCHLEIFE");
	System.out.println("");
	System.out.print("Bitte eine Ganzzahl eingeben: ");
	int nutzerZahl = eingabe.nextInt();
	ausgabeVorwaerts(nutzerZahl);
	ausgabeRueckwaerts(nutzerZahl);
	summeBisN(nutzerZahl);
	summeZweiN(nutzerZahl);
	summeZweiNPlusEins(nutzerZahl);
	moduloSieben();
	moduloVier();
	eingabe.close();
	}

	//AUSGABE: VORW�RTS-SCHLEIFE
	public static void ausgabeVorwaerts(int nutzerZahl) {
		System.out.println();
		System.out.println("*** Aufgabe a): Vorw�rtsz�hlende Schleife ***");
		System.out.println();
		System.out.print("Schleife: ");
		//Die for-Schleife dient dem Weglassen eines Kommas am Ende der Ausgabe
		for(int zaehlerVariable = 1; zaehlerVariable <= nutzerZahl; zaehlerVariable+=1) {
			if(zaehlerVariable == nutzerZahl) {
				System.out.print(zaehlerVariable);
			}
			else {
				System.out.print(zaehlerVariable+", ");	
			}
		}
		System.out.println();
	}

	//AUSGABE: R�CKW�RTS-SCHLEIFE
	public static void ausgabeRueckwaerts(int nutzerZahl) {
		System.out.println();
		System.out.println("*** Aufgabe b): Vorw�rtsz�hlende Schleife ***");
		System.out.println();
		System.out.print("Schleife: ");
		//Die for-Schleife dient dem Weglassen eines Kommas am Ende der Ausgabe
		for(int zaehlerVariable = nutzerZahl; zaehlerVariable > 0; zaehlerVariable-=1) {
			if(zaehlerVariable == 1) {
				System.out.print(zaehlerVariable);
			}
			else {
				System.out.print(zaehlerVariable+", ");	
			}
		}
		System.out.println();
	}

/*
	Aufgabe 2: Summe
	Geben Sie in der Konsole die Summe der Zahlenfolgen aus. Erm�glichen Sie es dem
	Benutzer die Zahl n festzulegen, welche die Summierung begrenzt.
	a) 1 + 2 + 3 + 4 +�+ n for-Schleife / while-Schleife --> f�r 8: 36
	b) 2 + 4 + 6 +�+ 2n for-Schleife / while-Schleife --> f�r 8: 20
	c) 1 + 3 + 5 +�+ (2n+1) for-Schleife / while-Schleife --> f�r 8 (17): 81
*/

	//AUSGABE: SUMME ALLER ZAHLEN BIS ZUR EINGEGEBENEN ZAHL
	public static void summeBisN(int nutzerZahl) {
		int summeBisN = 0;
		System.out.println();
		System.out.println("*** Aufgabe 2 - Teilaufgabe A ***");
		System.out.println();
		System.out.println("Es wurde folgende Zahl zur Begrenzung eingegeben: " + nutzerZahl);
		System.out.println();
		for(int schleifenVariable = 1; schleifenVariable <= nutzerZahl; schleifenVariable++) {
			summeBisN = summeBisN + schleifenVariable;
		}
		System.out.println("Die Summe von 0 bis zu " + nutzerZahl + " betr�gt: " + summeBisN);
	}

	//AUSGABE: SUMME DER GERADEN ZAHLEN BIS ZUR EINGEGEBENEN ZAHL
	public static void summeZweiN(int nutzerZahl) {
		int summeZweiN = 0;
		System.out.println();
		System.out.println("*** Aufgabe 2 - Teilaufgabe B ***");
		System.out.println();
		System.out.println("Es wurde folgende Zahl zur Begrenzung eingegeben: " + nutzerZahl);
		System.out.println();
		for(int schleifenVariable = 2; schleifenVariable <= nutzerZahl; schleifenVariable += 2) {
			summeZweiN = summeZweiN + schleifenVariable;
		}
		System.out.println("Die Summe von 0 bis zu " + nutzerZahl + " betr�gt: " + summeZweiN);
	}
	
	//AUSGABE: SUMME ALLER UNGERADEN ZAHLEN BIS ZUR EINGEGEBENEN ZAHL
	public static void summeZweiNPlusEins(int nutzerZahl) {
		int summeZweiNPlusEins = 0;
		nutzerZahl = ((nutzerZahl * 2)+1);
		System.out.println();
		System.out.println("*** Aufgabe 2 - Teilaufgabe C ***");
		System.out.println();
		System.out.println("Es wurde folgende Zahl zur Begrenzung eingegeben: " + nutzerZahl);
		System.out.println();
		for(int schleifenVariable = 1; schleifenVariable <= nutzerZahl; schleifenVariable+=2) {
			summeZweiNPlusEins = summeZweiNPlusEins + schleifenVariable;
		}
		System.out.println("Die Summe von 0 bis zu " + nutzerZahl + " betr�gt: " + summeZweiNPlusEins);
	}
	
/*
	Schreibe ein Programm, das f�r alle Zahlen zwischen 1 und 200 testet:
	* ob sie durch 7 teilbar sind.
	* nicht durch 5 aber durch 4 teilbar sind.
	Lasse jeweils die Zahlen, auf die die Bedingungen zutreffen ausgeben.
*/

	//AUSGABE: MODULO -> PR�FE OB EINE ZAHL DURCH 7 TEILBAR IST (immer wenn modulo 0 als Ergebnis liefert)
	public static void moduloSieben() {
		System.out.println();
		System.out.println("*** Modulo-Aufgabe: durch 7 teilbar ***");
		System.out.println();
		for(int schleifenVariable = 1; schleifenVariable <= 200 ; schleifenVariable++) {
			if((schleifenVariable % 7) == 0) {
				System.out.println("Die Zahl " + schleifenVariable + " ist restlos durch 7 teilbar.");
			}
			else {
				//System.out.println("Die Zahl " + schleifenVariable + " ist NICHT restlos durch 4 teilbar.");				
			}
		}
	}

	//AUSGABE: MODULO -> PR�FE OB EINE ZAHL DURCH 4 TEILBAR IST, ABER NICHT DURCH 5
	public static void moduloVier() {
		System.out.println();
		System.out.println("*** Modulo-Aufgabe: durch 4 teilbar, aber nicht durch 5 teilbar ***");
		System.out.println();
		for(int schleifenVariable = 1; schleifenVariable <= 200 ; schleifenVariable++) {
			if((schleifenVariable % 4) == 0 && (schleifenVariable % 5) != 0) {
				System.out.println("Die Zahl " + schleifenVariable + " ist restlos durch 4 teilbar, aber nicht durch 5 teilbar.");
			}
			else {
				//System.out.println("Die Zahl " + schleifenVariable + " ist NICHT restlos durch 4 teilbar.");				
			}
		}
		
	}
}