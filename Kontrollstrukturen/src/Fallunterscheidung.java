import java.util.Scanner;

/*
*	AUFGABENSTELLUNG:
*	Erstellen Sie die Konsolenanwendung Noten. Das Programm Noten soll nach der Eingabe
*	einer Ziffer die sprachliche Umschreibung ausgeben (1 = Sehr gut, 2 = Gut, 3 = Befriedigend,
*	4 = Ausreichend, 5 = Mangelhaft, 6 = Ungen�gend). Falls eine andere Ziffer eingegeben
*	wird, soll ein entsprechender Fehlerhinweis ausgegeben werden. 
*/
public class Fallunterscheidung {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);	// Scanner erstellen f�r die Noteneingabe durch den Nutzer
		System.out.println("AUFGABE: FALLUNTERSCHEIDUNG MIT SWITCH UND IF");
		System.out.println("");
		System.out.print("Bitte eine Note eingeben: ");		// Konsolenausgabe zur Nutzereingabeaufforderung
		int note = eingabe.nextInt();		// Note wird durch den Nutzer eingegeben und der Variable note zugewiesen
		System.out.println();
		ausgabeIf(note);			// Methode f�r Ausgabe mittels if, else und else if
		ausgabeSwitch(note);		// Methode f�r Ausgabe mittels switch
		eingabe.close();
	}
			
	// Methode zur Konsolenausgabe mit if, else und else if
	public static void ausgabeIf(int note) {
		System.out.println();
		System.out.println("Ihre Eingabe: " + note);
		System.out.println();
			
		if (note == 1) {
			System.out.println("Sehr gut");				
		}
		else if (note == 2) {
			System.out.println("Gut");				
		}
		else if (note == 3) {
			System.out.println("Befriedigend");				
		}
		else if (note == 4) {
			System.out.println("Ausreichend");				
		}
		else if (note == 5) {
			System.out.println("Mangelhaft");				
		}
		else if (note == 6) {
			System.out.println("Ungen�gend");				
		}
		else {
			System.out.println("Fehlerhaft");	
		}	
	}

	// Methode zur Konsolenausgabe mit switch
	public static void ausgabeSwitch(int note) {
		System.out.println();
		System.out.println("Ihre Eingabe: " + note);
		System.out.println();
		
		switch(note) {
			case 1:
				System.out.println("Sehr gut.");
				break;
			case 2:
				System.out.println("Gut.");
				break;
			case 3:
				System.out.println("Befriedigend.");
				break;
			case 4:
				System.out.println("Ausreichend.");
				break;
			case 5:
				System.out.println("Mangelhaft.");
				break;
			case 6:
				System.out.println("Ungen�gend.");
				break;
			default:
				System.out.println("Fehlerhafte Eingabe");
				break;
		}
		
	}
}
