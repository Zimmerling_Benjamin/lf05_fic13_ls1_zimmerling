import java.util.Scanner;

public class Auswahlstruktur {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		System.out.println("AUFGABE: AUSWAHLSTRUKTUR");
		System.out.println("");
		System.out.print("Bitte die erste Zahl eingeben: ");
		int zahlEins = eingabe.nextInt();
		System.out.print("Bitte die zweite Zahl eingeben: ");
		int zahlZwei = eingabe.nextInt();
		System.out.print("Bitte die dritte Zahl eingeben: ");
		int zahlDrei = eingabe.nextInt();
	
		aufgabe1();
		aufgabe2(zahlEins, zahlZwei);
		aufgabe3(zahlEins, zahlZwei);
		aufgabe4(zahlEins, zahlZwei);
		aufgabe5(zahlEins, zahlZwei, zahlDrei);
		aufgabe6(zahlEins, zahlZwei, zahlDrei);
		aufgabe7(zahlEins, zahlZwei, zahlDrei);
	
		eingabe.close();
	}

	// 1. Nennen Sie Wenn-Dann-Aktivit�ten aus ihrem Alltag.
	// 1. Alltagsbeispiel
	public static void aufgabe1 () {
		System.out.println();
		System.out.println("*** Teilaufgabe 1 ***");
		System.out.println();
		System.out.println("Beispiel 1: Wenn kein Essen da ist, dann gehe einkaufen.");
		System.out.println();
		/*
		 * for Schleife ver�ndert die Zeichenkette, damit ich beide F�lle ber�cksichtigen kann
		 * bei zaehlvariable = 0 erh�lt die Variable kuehlschrank die Zeichenkette "Gef�llt"
		 * bei zaehlvariable = 1 erh�lt die Variable kuehlschrank die Zeichenkette "Ungef�llt"
		 * Je nach Fall (case) wird dann die If-Verzweigung eine entsprechende Antwort liefern
		 */
		for(int zaehlvariable = 0; zaehlvariable < 2; zaehlvariable += 1) {
			String kuehlschrank = "g";
			switch(zaehlvariable) {
			case 0:
				kuehlschrank = "Gef�llt";
				break;
			case 1:
				kuehlschrank = "Ungef�llt";
				break;
			default:
				System.out.println("Fehler bei Ermittlung der Variable zaehlervariable.");
				break;
			}
			if(kuehlschrank == "Ungef�llt") {
				System.out.println("Der Wert der Variable kuehlschrank: " + kuehlschrank + ". Der K�hlschrank ist leer. Gehe einkaufen.");
			}
			else if (kuehlschrank == "Gef�llt") {
				System.out.println("Der Wert der Variable kuehlschrank: " + kuehlschrank + ". Der K�hlschrank ist voll. Ab auf die Couch.");
			}
			else {
				System.out.println("Fehler bei der Ermittlung des Wertes f�r die Variable kuehlschrank.");
			}
		}
		// 2. Alltagsbeispiel
		System.out.println();
		System.out.println("Beispiel 2: Wenn es regnet, dann nimm den Regenschirm mit.");
		System.out.println();
		/*
		 * for Schleife ver�ndert die Zeichenkette, damit ich beide F�lle ber�cksichtigen kann
		 * bei zaehlvariable = 0 erh�lt die Variable wetter die Zeichenkette "Regen"
		 * bei zaehlvariable = 1 erh�lt die Variable wetter die Zeichenkette "Sonne"
		 * Je nach Fall (case) wird dann die If-Verzweigung eine entsprechende Antwort liefern
		 */
		for(int zaehlvariable = 0; zaehlvariable < 2; zaehlvariable += 1) {
			String wetter = "g";
			switch (zaehlvariable) {
			case 0:
				wetter = "Regen";
				break;
			case 1:
				wetter = "Sonne";
				break;
			}
			if (wetter == "Regen") {
				System.out.println("Der Wert der Variable wetter: " + wetter + ". Es regnet. Nimm den Regenschirm mit");
			}
			else if (wetter == "Sonne") {
				System.out.println("Der Wert der Variable wetter: " + wetter + ". Es ist sonnig. Der Schirm hat Urlaub.");
			}
			else {
				System.out.println(".Fehler bei der Ermittlung des Wertes f�r die Variable wetter.");
			}
		}
		// 3. Alltagsbeispiel
		System.out.println();
		System.out.println("Beispiel 3: Wenn Wochenende ist, stelle den Wecker aus.");
		System.out.println();
		/*
		 * for Schleife ver�ndert die Zeichenkette, damit ich alle F�lle ber�cksichtigen kann
		 * bei zaehlvariable = 0 erh�lt die variable kuehlschrank die Zeichenkette "Montag"
		 * bei zaehlvariable = 1 erh�lt die variable kuehlschrank die Zeichenkette "Dienstag"
		 * bei zaehlvariable = 2 erh�lt die variable kuehlschrank die Zeichenkette "Mittwoch"
		 * bei zaehlvariable = 3 erh�lt die variable kuehlschrank die Zeichenkette "Donnerstag"
		 * bei zaehlvariable = 4 erh�lt die variable kuehlschrank die Zeichenkette "Freitag"
		 * bei zaehlvariable = 5 erh�lt die variable kuehlschrank die Zeichenkette "Samstag"
		 * bei zaehlvariable = 1 erh�lt die variable kuehlschrank die Zeichenkette "Sonntag"
		 * Je nach Fall (case) wird dann die If-Verzweigung eine entsprechende Antwort liefern
		 */
		for(int zaehlvariable = 0; zaehlvariable < 7 ; zaehlvariable += 1) {
			String wochentag = "g";
			switch(zaehlvariable) {
			case 0:
				wochentag = "Montag";
				break;
			case 1:
				wochentag = "Dienstag";
				break;
			case 2:
				wochentag = "Mittwoch";
				break;
			case 3:
				wochentag = "Donnerstag";
				break;
			case 4:
				wochentag = "Freitag";
				break;
			case 5:
				wochentag = "Samstag";
				break;
			case 6:
				wochentag = "Sonntag";
				break;
			}
			//Die If-Verzweigung pr�ft durch logisches Oder (||) ob der Folgetag einen Wecker ben�tigen w�rde oder nicht
			if(wochentag == "Freitag" || wochentag == "Samstag") {
				System.out.println("Es ist " + wochentag + ". Zeit f�r Langschl�fer.");
			}
			else if (wochentag == "Sonntag" || wochentag == "Montag" || wochentag == "Dienstag" || wochentag == "Mittwoch" || wochentag == "Donnerstag") {
				System.out.println("Es ist " + wochentag + ". Wecker f�r morgen stellen.");
			}
		}
	}	
	
	// 2. Wenn beide Zahlen gleich sind, soll eine Meldung ausgegeben werden (If)
	public static void aufgabe2 (int zahlEins, int zahlZwei) {
		System.out.println();
		System.out.println("*** Teilaufgabe 2 ***");
		System.out.println();
		if(zahlEins == zahlZwei) {
			System.out.println("Die eingegebenen Zahlen " + zahlEins + " und " + zahlZwei + " sind gleich.");
		}
	}
	
	// 3. Wenn die 2. Zahl gr��er als die 1. Zahl ist, soll eine Meldung ausgegeben werden (If)
	public static void aufgabe3 (int zahlEins, int zahlZwei) {
		System.out.println();
		System.out.println("*** Teilaufgabe 3 ***");
		System.out.println();
		if(zahlEins < zahlZwei) {
			System.out.println("Die zweite Zahl (" + zahlZwei + ") ist gr��er als die erste Zahl (" + zahlEins + ").");
		}
	}
	
	// 4. Wenn die 1. Zahl gr��er oder gleich als die 2. Zahl ist, soll eine Meldung ausgegeben werden, ansonsten eine andere Meldung (If-Else)
	public static void aufgabe4 (int zahlEins, int zahlZwei) {
		System.out.println();
		System.out.println("*** Teilaufgabe 4 ***");
		System.out.println();
		if(zahlEins >= zahlZwei) {
			System.out.println("Die erste Zahl (" + zahlEins + ") ist gr��er als die zweite Zahl (" + zahlZwei + ") oder gleich gro�.");
		}
	}

	// 5. Wenn die 1.Zahl gr��er als die 2.Zahl und die 3. Zahl ist, soll eine Meldung ausgegeben werden (If mit && / Und)
	public static void aufgabe5 (int zahlEins, int zahlZwei, int zahlDrei) {
		System.out.println();
		System.out.println("*** Teilaufgabe 5 ***");
		System.out.println();
		if(zahlEins > zahlZwei && zahlEins > zahlDrei) {
			System.out.println("Die erste Zahl (" + zahlEins + ") ist gr��er als die zweite (" + zahlZwei + ") und dritte Zahl (" + zahlDrei + ").");
		}
	}

	// 6. Wenn die 3.Zahl gr��er als die 2.Zahl oder die 1. Zahl ist, soll eine Meldung ausgegeben werden (If mit || / Oder)
	public static void aufgabe6 (int zahlEins, int zahlZwei, int zahlDrei) {
		System.out.println();
		System.out.println("*** Teilaufgabe 6 ***");
		System.out.println();
		if(zahlDrei > zahlEins || zahlDrei > zahlZwei) {
			System.out.println("Die dritte Zahl (" + zahlDrei + ") ist gr��er als die erste (" + zahlEins + ") oder zweite Zahl (" + zahlZwei + ").");
		}
	}

	// 7. Geben Sie die gr��te der 3 Zahlen aus. (If-Else mit && / Und)
	public static void aufgabe7 (int zahlEins, int zahlZwei, int zahlDrei) {
		System.out.println();
		System.out.println("*** Teilaufgabe 7 ***");
		System.out.println();
		if(zahlEins > zahlZwei && zahlEins > zahlDrei) {
			System.out.println("Die erste Zahl (" + zahlEins + ") ist gr��er als die zweite (" + zahlZwei + ") und dritte Zahl (" + zahlDrei + ").");
		}
		if(zahlZwei > zahlEins && zahlZwei > zahlDrei) {
			System.out.println("Die zweite Zahl (" + zahlZwei + ") ist gr��er als die erste (" + zahlEins + ") und dritte Zahl (" + zahlDrei + ").");
		}
		if(zahlDrei > zahlZwei && zahlDrei > zahlEins) {
			System.out.println("Die dritte Zahl (" + zahlDrei + ") ist gr��er als die erste (" + zahlEins + ") und zweite Zahl (" + zahlZwei + ").");
		}
	}
	
}
