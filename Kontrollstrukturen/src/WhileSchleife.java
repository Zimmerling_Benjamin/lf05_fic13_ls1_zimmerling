import java.util.Scanner;

/*
*  AUFGABENSTELLUNG
*  Geben Sie in der Konsole die nat�rlichen Zahlen von 1 bis n heraufz�hlend (bzw. von n bis 1
*  herunterz�hlend) aus. Erm�glichen Sie es dem Benutzer die Zahl n festzulegen. Nutzen Sie
*  zur Umsetzung eine while-Schleife.
*  a) 1, 2, 3, �, n while-Schleife
*  b) n, �, 3, 2, 1 while-Schleife
*/

public class WhileSchleife {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		System.out.println("AUFGABE: WHILE-SCHLEIFE");
		System.out.println("");
		System.out.print("Bitte eine Ganzzahl eingeben: ");
		int nutzerZahl = eingabe.nextInt();
		ausgabeVorwaerts(nutzerZahl);
		ausgabeRueckwaerts(nutzerZahl);
		fakultaet(nutzerZahl);
		eingabe.close();
		}

		public static void ausgabeVorwaerts(int nutzerZahl) {
			System.out.println();
			System.out.println("*** Aufgabe a): Vorw�rtsz�hlende Schleife ***");
			System.out.println();
			System.out.print("Schleife: ");
			int zaehlerVariable = 1;
			while (zaehlerVariable <= nutzerZahl) {
				if(zaehlerVariable == nutzerZahl) {
					System.out.print(zaehlerVariable);
				}
				else {
					System.out.print(zaehlerVariable+", ");	
				}
				zaehlerVariable +=1;
			}
			System.out.println();
		}

		public static void ausgabeRueckwaerts(int nutzerZahl) {
			System.out.println();
			System.out.println("*** Aufgabe b): Vorw�rtsz�hlende Schleife ***");
			System.out.println();
			System.out.print("Schleife: ");
			while (nutzerZahl > 0) {
				if(nutzerZahl == 1) {
					System.out.print(nutzerZahl);
				}
				else {
					System.out.print(nutzerZahl+", ");	
				}
				nutzerZahl-=1;
			}
			System.out.println();
		}

/*
		Schreiben Sie ein Programm, das zu einer Zahl n <= 20 die Fakult�t n! ermittelt.
				Es gilt:
				n! = 1 * 2 * ... * (n-1) * n sowie 0! = 1
				z.B. ist 3! = 1 * 2 * 3 = 6
*/
		
		public static void fakultaet(int nutzerZahl) {
			int produkt = 1;
			int durchgangsZaehler = 1;
			System.out.println();
			System.out.println("*** Aufgabe zur Fakult�t ***");
			System.out.println();
			if(nutzerZahl > 10) {				// Zahl wird relativ schnell riesig gro�, das kann der Datentyp nicht leisten --> Fehler abfangen
				System.out.println("Bitte eine Zahl kleiner oder gleich 10 eingeben. Danke.");
			}
			else {
				if(nutzerZahl == 0) {
					produkt = produkt * 1; // da 0! = 1
				}
				else {
					while(durchgangsZaehler <= nutzerZahl) {
						produkt = produkt * durchgangsZaehler;
						durchgangsZaehler += 1;
					}	
				}
				System.out.println("Das Produkt der Zahlen bis zur " + nutzerZahl + " ist: " + produkt + ".");
			}
		}
}
