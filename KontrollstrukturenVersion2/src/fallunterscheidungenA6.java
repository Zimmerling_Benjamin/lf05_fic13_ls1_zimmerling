import java.util.Scanner;

/*
Aufgabe 6: R�mische Zahlen 2

Erstellen Sie eine Konsolenanwendung Rom. Das Programm Rom soll nach der Eingabe einer
r�mischen Zahl, bestehend aus mehreren r�mischen Zeichen die entsprechende Dezimalzahl
ausgeben (I = 1, V = 5, X = 10, L = 50, C = 100, D = 500, M = 1000). Eine Verarbeitung soll
bis zur Dezimalzahl 3999 m�glich sein.

Eine weitere Herausforderung besteht darin, die eingegebene r�mische Zahl auf Korrektheit
zu pr�fen.

Die Regeln f�r r�mische Zahlen lauten:

1. Additionsregel: Alle Zeichen nebeneinander werden addiert.
Kleinere Zahlen folgen gr��eren, also zum Beispiel XVII = 10+5+1+1 = 17.

2. Maximal 3 gleiche Zeichen: Es d�rfen nur maximal 3 gleiche Zeichen aufeinander folgen
(also: III, XXX, CCC oder MMM).
Die Zahlzeichen V, L, D stehen nie mehrfach, denn bspw. VV w�re ja X.

3. Subtraktionsregel: Steht ein kleines Zahlzeichen (wie I) vor einem gr��eren (wie V), so
wird es abgezogen.
Also 4 w�re IV (1 vor 5).
Es darf immer nur ein Zeichen vorangestellt werden (erlaubt sind I, X und C).

4. Reihenfolge bei Subtraktion: Eine bestimmte Reihenfolge ist bei der Subtraktion
einzuhalten:

I (1) darf nur von V (5) und X (10) abgezogen werden. Erlaubt sind also nur IV und IX.

X (10) darf nur von L (50) und C (100) abgezogen werden. Erlaubt sind also nur XL und
XC.

C (100) darf nur von D (500) und M (1000) abgezogen werden. Erlaubt sind also nur CD

und CM.
*/
public class fallunterscheidungenA6 {

	public static void main() {
		int zahlenWert = zahlenErmittlung();
		berechneRoemischeZahl(zahlenWert);
	}
	
	public static int zahlenErmittlung() {
		boolean zahlenPruefwert = true;
		int zahlenWert = 0;
		while(zahlenPruefwert) {
			System.out.print("Bitte eine ganze Zahl zwischen 0 und 3.999 eingeben: ");
			Scanner zahlenEingabe = new Scanner(System.in);
			zahlenWert = zahlenEingabe.nextInt();
			if(zahlenWert > 0 && zahlenWert < 4000) {
				zahlenPruefwert = false;				
			}
		}
		return zahlenWert;
	}
	
	public static void berechneRoemischeZahl(int zahlenwert) {
		int zahl = 0;
		int[] zahlenarray = new int[4];
		int zaehler = 0;
		while (zahl > 0) {
			zahlenarray[zaehler] = zahl % (10 * (zaehler + 1));
			zaehler += 1;
		}
		
		for(zaehler = 0; zaehler <= zahlenarray.length; zaehler ++) {
			int sum = 0; 
			int temp = (int) (zahlenarray[zaehler]/Math.pow(zahlenarray[zaehler], zaehler));
			System.out.println(temp);
			sum = sum + (zahlenarray[zaehler] - temp);
		}
	}
	
	
	
}
