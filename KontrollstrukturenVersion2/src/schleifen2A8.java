/*
AB "Schleifen 2": Aufgabe 8 ("Matrix")
Es soll eine Multiplikationsmatrix auf dem Bildschirm (von 10 Zeilen / 10 Spalten) angezeigt
werden. Nach Eingabe einer Ziffer zwischen 2 und 9 werden alle Zahlen durch einen Stern
gekennzeichnet, die diese Ziffer enthalten, durch die eingegebene Zahl ohne Rest teilbar
sind oder der Quersumme entsprechen
*/

import java.util.Scanner;

public class schleifen2A8 {

	//MAIN-METHODE
	public static void main(String[] args) {
		int zahlEins = zahlenEingabe();
		int zahlZwei = zahlenEingabe();
		ausgabeMatrix(zahlEins, zahlZwei);
	}

	//METHODE zur Zahleneingabe durch den Nutzer
	public static int zahlenEingabe() {
		boolean pruefWert = true;
		int minimum = 1;
		int maximum = 10;
		int nutzerZahl = 0;
		while(pruefWert) {
			System.out.print("Bitte eine Zahl zwischen 2 und 9 eingeben: ");
			Scanner nutzerEingabe = new Scanner(System.in);
			nutzerZahl = nutzerEingabe.nextInt();
			if(nutzerZahl > minimum && nutzerZahl < maximum) {
				pruefWert = false;
			}
			else {
				System.out.println("Sie haben eine falsche Eingabe get�tigt.");
			}
		}
		return nutzerZahl;
	}
	
	//METHODE zur Ausgabe der Matrix
	public static void ausgabeMatrix(int zahlEins, int zahlZwei) {
		int minimum = 1;
		int maximum = 10;
		int temp = 0;
		for(int zeile = minimum; zeile <= maximum; zeile ++) {
			for(int spalte = minimum; spalte <= maximum; spalte ++) {
				temp = (((zeile-1)*10)+(spalte-1));
				int stelleEins = temp % 10;
				int stelleZwei = (temp/10) % 10;
				int quersumme = stelleEins + stelleZwei;
				if(temp == 0) {
					System.out.printf("%5d", temp);
				}
				else if(stelleEins == zahlEins || stelleEins == zahlZwei || stelleZwei == zahlEins || stelleZwei == zahlZwei || temp % zahlEins == 0 || temp % zahlZwei == 0 || quersumme == zahlEins || quersumme == zahlZwei) {
					System.out.printf("%5s", "*");
				}
				else {
					System.out.printf("%5d", temp);	
				}
			}
			System.out.println();
		}
	}	
}
