/*
AB "Fallunterscheidungen": Aufgabe 5 ("Ohmsches Gesetz")
Nach dem Ohmschen Gesetz berechnet sich der Widerstand eines ohmschen Widerstandes
mit: R=U/I
Schreiben Sie ein Programm, in das der Benutzer zun�chst �ber die Eingabe der
Buchstaben R, U oder I ausw�hlen kann, welche Gr��e berechnet werden soll. Gibt er einen
falschen Buchstaben ein, soll eine Meldung �ber die Fehleingabe erfolgen.
Anschlie�end soll er die Werte der fehlenden Gr��en eingeben. Am Ende gibt das
Programm den Wert der gesuchten Gr��e mit der richtigen Einheit aus.
*/

import java.util.Scanner;

public class fallunterscheidungenA5 {

	public static void main(String[] args) {
		char nutzerAuswahl = nutzerAuswahl();
		double stromstaerke = 0.0;
		double spannung = 0.0;
		double widerstand = 0.0;
		switch(nutzerAuswahl) {
		case 'R':
			stromstaerke = ermittleStromstaerke(); 
			spannung = ermittleSpannung();
			berechneWiderstand(stromstaerke, spannung);
			break;
		case 'U':
			stromstaerke = ermittleStromstaerke(); 
			widerstand = ermittleWiderstand();
			berechneSpannung(stromstaerke, widerstand);
			break;
		case 'I':
			spannung = ermittleSpannung();
			widerstand = ermittleWiderstand();
			berechneStromstaerke(spannung, widerstand);
			break;
		default:
			System.out.println("Fehler. Bitte erneut starten.");
		}
	}
	
	public static char nutzerAuswahl() {
		boolean endlos = true;
		char nutzerAuswahl = 'A';
		while(endlos) {
			System.out.print("Bitte die zu berechnende Gr��e ausw�hlen: \"R\" f�r Widerstand, \"U\" f�r Spannung oder \"I\" f�r Stromst�rke. ");
			Scanner nutzerEingabe = new Scanner(System.in);
			nutzerAuswahl = nutzerEingabe.next().charAt(0);
			java.lang.Character.toUpperCase(nutzerAuswahl);
			if(nutzerAuswahl == 'R' || nutzerAuswahl == 'U' || nutzerAuswahl != 'I') {
				endlos = false;
			}
			else {
				System.out.println("Sie haben eine ung�ltige Auswahl getroffen.");
			}
		}
		return nutzerAuswahl;
	}
	
	public static double ermittleStromstaerke() {
		System.out.print("Bitte den Wert f�r die Stromst�rke (I) in Ampere (A) eingeben: ");
		Scanner stromstaerkeScanner = new Scanner(System.in);
		double stromstaerkeWert = stromstaerkeScanner.nextDouble();
		return stromstaerkeWert;
	}
	
	public static double ermittleSpannung() {
		System.out.print("Bitte den Wert f�r die Spannung (U) in Volt (V) eingeben: ");
		Scanner spannungScanner = new Scanner(System.in);
		double spannungWert = spannungScanner.nextDouble();
		return spannungWert;
	}	
	
	public static double ermittleWiderstand() {
		System.out.print("Bitte den Wert f�r Widerstand (R) in Ohm eingeben: ");
		Scanner widerstandScanner = new Scanner(System.in);
		double widerstandWert = widerstandScanner.nextDouble();
		return widerstandWert;
	}
	
	public static void berechneWiderstand(double stromstaerke, double spannung) {
		double widerstand = spannung/stromstaerke;
		System.out.print("Der Widerstand betr�gt: " + widerstand);
	}

	public static void berechneSpannung(double stromstaerke, double widerstand) {
		double spannung = widerstand * stromstaerke;
		System.out.print("Die Spannung betr�gt: " + spannung);
	}
	
	public static void berechneStromstaerke(double spannung, double widerstand) {
		double stromstaerke = spannung/widerstand;
		System.out.println("Die Stromst�rke betr�gt: " + stromstaerke);
	}
	
}