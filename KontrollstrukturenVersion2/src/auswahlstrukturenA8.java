
/*
AB "Auswahlstrukturen":und Aufgabe 8 ("Schaltjahr")
Entwickeln Sie ein Programm, welches ausgibt, ob das eingegebene Jahr ein Schaltjahr ist.

Regel 1: Ist eine Jahreszahl ganzzahlig durch 4 teilbar, dann ist das Jahr ein Schaltjahr mit
366 Tagen.
Beispiele: 1980, 1972, 1720 waren Schaltjahre

Regel 2: Ausnahme von Regel 1 sind alle Jahreszahlen, die nach Regel 1 ein Schaltjahr
sind, aber deren Jahreszahl ganzzahlig durch 100 teilbar sind.
Beispiele: 1700, 1800 und 1900 oder ferner 2100 sind keine Schaltjahre.

Regel 3: Ausnahme von Regel 2 sind alle Jahreszahlen, die nach Regel 2 kein Schaltjahr
sind, aber deren Jahreszahl ganzzahlig durch 400 teilbar.
Beispiele: 1600 und 2000 waren Schaltjahre zu 366 Tagen.

oder eine einfach zu merkende Regel:
  Ein Schaltjahr ist alle vier Jahre (1992 und 1996 waren z.B. Schaltjahre),
  alle hundert Jahre nicht (1700 und 1800 waren keine Schaltjahre),
  und alle vierhundert Jahre doch (2000 war doch ein Schaltjahr),
  Dummerweise wurde diese Regelung erst 1582 eingef�hrt, d.h. davor galt nur Regel1,
  Eingef�hrt wurden Schaltjahre im heutigen Sinne durch Julius C�sar 45 v. Chr.
Das Programm soll vom Benutzer eine Jahreszahl abfragen. Am Ende des Programms soll
ausgegeben werden, ob es sich um ein Schaltjahr handelt oder nicht.
*/

//IMPORT
import java.util.Scanner;

public class auswahlstrukturenA8 {

	//MAIN-METHODE
	public static void main(String[] args) {
		int jahresZahl = ermittleJahreszahl();
		boolean schaltjahr = pruefeJahreszahl(jahresZahl);
		ausgabe(schaltjahr, jahresZahl);
	}
	
	//METHODE zur Ermittlung der Jahreszahl durch den Nutzer
	public static int ermittleJahreszahl() {
		boolean eingabePruefung = true;
		int jahresZahl = 1000;
		while(eingabePruefung) {
			System.out.print("Bitte geben Sie eine g�ltige Jahreszahl ein: ");
			Scanner nutzerEingabe = new Scanner(System.in);
			jahresZahl = nutzerEingabe.nextInt();
			if(jahresZahl >999 && jahresZahl <10000) {
				eingabePruefung = false;
			}
		}
		return jahresZahl;
	}
	
	//METHODE zur Schaltjahrpr�fung
	public static boolean pruefeJahreszahl(int jahresZahl) {
		boolean schaltjahr = false;
		if(jahresZahl < 1582) {
			if(jahresZahl % 4 == 0) {
				schaltjahr = true;
			}
			else {
				schaltjahr = false;
			}
		}
		else {
			if(jahresZahl % 4 == 0) {
				schaltjahr = true;
				if(jahresZahl % 100 == 0 && jahresZahl % 400 != 0) {
					schaltjahr = false;
				}
			}	
		}
		return schaltjahr;
	}
	
	//METHODE zur Ausgabe
	public static void ausgabe(boolean schaltjahr, int jahresZahl) {
		if(schaltjahr == false) {
			System.out.println("Das Jahr " + jahresZahl + " ist kein Schaltjahr.");
		}
		else {
			System.out.println("Das Jahr " + jahresZahl + " ist ein Schaltjahr.");			
		}
	}
	
}
