/*
AB "Schleifen 1": Aufgabe 8 ("Quadrat")
Schreiben Sie ein Programm, dass die Seitenl�nge eines Quadrats erfragt und dann im
Textmodus ein Quadrat dieser Gr��e ausgibt. 
*/

import java.util.Scanner;

public class schleifen1A8 {
	
	public static void main(String[] args) {
		int maximum = zahlenEingabe();
		ausgabe(maximum);

	}
	
	public static int zahlenEingabe() {
		boolean wahrheit = true;
		int zahlenEingabe = 0;
		while(wahrheit) {
			System.out.print("Bitte eine ganze Zahl eingeben: ");
			Scanner nutzerEingabe = new Scanner(System.in);
			zahlenEingabe = nutzerEingabe.nextInt();
			if(zahlenEingabe > 0) {
				wahrheit = false;
			}
		}	
		return zahlenEingabe;
	}
	
	public static void ausgabe(int maximum) {
		for(int zeile = 1; zeile <= maximum; zeile ++) {
			for(int spalte = 1; spalte <= maximum; spalte ++) {
				if(zeile == 1) {
					System.out.printf("%2s", "*");
				}
				else if(zeile == maximum) {
					System.out.printf("%2s", "*");
				}
				else if(spalte == 1 || spalte == maximum) {
					System.out.printf("%2s", "*");
				}
				else {
					System.out.printf("%2s", " ");
				}
			}
			System.out.println();
		}
	}

}
