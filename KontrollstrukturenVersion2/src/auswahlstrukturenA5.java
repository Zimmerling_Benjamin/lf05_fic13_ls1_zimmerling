
/*

AB "Auswahlstrukturen": Aufgabe 5 --> BMI
BMI (Body Mass Index): Der BMI berechnet sich aus dem Körpergewicht [kg] dividiert durch
das Quadrat der Körpergröße [m2].
Die Formel lautet: BMI = (Körpergewicht in kg): (Körpergröße in m)2
.
Dies bedeutet, eine Person mit einer Körpergröße von 160 cm und einem Körpergewicht von
60 kg hat einen BMI von 23,4 [60: 2,56 = 23,4]. Der BMI einer Person wird nach folgenden
Regeln klassifiziert: 
 */

//IMPORT
import java.util.Scanner;

public class auswahlstrukturenA5 {
	
	//MAIN-METHODE
	public static void main(String[] args) {
		double koerperGroeße = ermittleKoerpergroeße();
		double koerperGewicht = ermittleKoerpergewicht();
		char geschlecht = ermittleGeschlecht();
		double bmi = berechneBMI(koerperGroeße, koerperGewicht);
		ausgabe(koerperGroeße, koerperGewicht, geschlecht, bmi);
	}
	
	//METHODE zur Ermittlung der Körpergröße durch Nutzereingabe
	public static double ermittleKoerpergroeße() {
		System.out.print("Bitte Ihre Körpergröße in m eingeben: ");
		Scanner nutzerEingabe = new Scanner(System.in);
		double koerperGroeße = nutzerEingabe.nextDouble();
		koerperGroeße = koerperGroeße/100;
		koerperGroeße = Math.pow(koerperGroeße, 2);
		return koerperGroeße;
	}
	
	//METHODE zur Ermittlung des Körpergewichts durch Nutzereingabe
	public static double ermittleKoerpergewicht() {
		System.out.print("Bitte Ihr Köpergewicht in kg eingeben: ");
		Scanner nutzerEingabe = new Scanner(System.in);
		double koerperGewicht = nutzerEingabe.nextDouble();
		return koerperGewicht;
	}
	
	//METHODER zur Ermittlung des Geschlechts durch Nutzereingabe
	public static char ermittleGeschlecht() {
		System.out.print("Bitte Ihr Geschlecht (m/w) eingeben: ");
		Scanner nutzerEingabe = new Scanner(System.in);
		char geschlecht = nutzerEingabe.next().charAt(0);
		geschlecht = java.lang.Character.toLowerCase(geschlecht);
		return geschlecht;
	}
	
	//METHODER zur Ermittlung des BMI durch Nutzereingabe
	public static double berechneBMI(double koerperGroeße, double koerperGewicht) {
		double bmi = koerperGewicht/koerperGroeße;
		bmi = Math.round(100.0 * bmi);
		bmi = bmi / 100.0;
		return bmi;
	}
	
	//METHODE zur Ausgabe für den Nutzer
	public static void ausgabe(double koerperGroeße, double koerpergewicht, char geschlecht, double bmi) {
		switch(geschlecht) {
		case 'm':
			if(bmi <25) {
				System.out.println();
				System.out.println("Ihr BMI liegt bei: " + bmi + ". Sie sind untergewichtigig.");
				System.out.println();
			}
			else if (bmi >25) {
				System.out.println();
				System.out.println("Ihr BMI liegt bei: " + bmi + ". Sie sind übergewichtig.");
				System.out.println();
			}
			else {
				System.out.println();
				System.out.println("Ihr BMI liegt bei: " + bmi + ". Sie sind normalgewichtigig.");
				System.out.println();
			}
			break;
		case 'w':
			if(bmi <25) {
				System.out.println();
				System.out.println("Ihr BMI liegt bei: " + bmi + ". Sie sind untergewichtigig.");
				System.out.println();
			}
			else if (bmi >25) {
				System.out.println();
				System.out.println("Ihr BMI liegt bei: " + bmi + ". Sie sind übergewichtig.");
				System.out.println();
			}
			else {
				System.out.println();
				System.out.println("Ihr BMI liegt bei: " + bmi + ". Sie sind normalgewichtigig.");
				System.out.println();
			}
			break;
		}	
	}
	
}
