/*
AB "Schleifen 1": Aufgabe 5 ("Einmaleins")
Sie sollen ein Programm entwickeln, welches das kleine
Einmaleins (1x1, 1x2 � bis 10x10) auf dem Bildschirm
ausgibt.
*/

public class schleifen1A5 {

	public static void main(String[] args) {
		int maximum = 11;
		for(int zeile = 1; zeile < maximum; zeile ++) {
			for(int spalte = 1; spalte < maximum; spalte ++) {
				System.out.printf("%4d", zeile*spalte);
			}
			System.out.println();
		}
	}
	
}
