/*
Aufgabe 4: Taschenrechner
Der Benutzer soll zwei Zahlen in das Programm eingeben, danach soll er entscheiden, ob die
Zahlen addiert, subtrahiert, multipliziert oder dividiert werden, diese Entscheidung soll �ber die
Eingabe der folgenden Symbole get�tigt werden: +, -, *, /
Nach der Auswahl soll das Ergebnis der Rechnung ausgegeben werden, bzw. eine
Fehlermeldung, falls eine falsche Auswahl getroffen wurde.
*/
import java.util.Scanner;

public class fallunterscheidungenA4 {

	//MAIN-METHODE
	public static void main(String[] args) {
		double zahlEins = zahlenEingabe();
		double zahlZwei = zahlenEingabe();
		rechnung(zahlEins, zahlZwei);
	}
	
	//METHODE zur Zahleneingabe durch den Nutzer
	public static double zahlenEingabe() {
		System.out.print("Bitte eine Zahl eingeben: ");
		Scanner nutzerEingabe = new Scanner(System.in);
		double zahl = nutzerEingabe.nextInt();
		return zahl;
	}
	
	//METHODE zur Festlegung des Operators, zur Berechnung und zur Datenausgabe
	public static void rechnung(double zahlEins, double zahlZwei) {
		boolean operatorPruefung = true;
		while(operatorPruefung) {
			Scanner nutzerEingabe = new Scanner(System.in);
			System.out.print("Bitte einen Operator (+, -, *, /) ausw�hlen: ");
			char operator = nutzerEingabe.next().charAt(0);
			double ergebnis = 0.0;
			if(operator == '+' || operator == '-' || operator == '*' || operator == '/') {
				switch(operator) {
				case '+':
					ergebnis = zahlEins + zahlZwei;
					System.out.print("Die Summe der Summanden " + zahlEins + " und " + zahlZwei + " ist: " + ergebnis);
					operatorPruefung = false;
					break;
				case '-':
					ergebnis = zahlEins - zahlZwei;
					System.out.print("Die Differenz von Minuend " + zahlEins + " und Subtrahend " + zahlZwei + " ist: " + ergebnis);
					operatorPruefung = false;
					break;
				case '*':
					ergebnis = zahlEins * zahlZwei;
					System.out.print("Das Produkt der Faktoren " + zahlEins + ", und " + zahlZwei + " ist: " + ergebnis);
					operatorPruefung = false;
					break;
				case '/':
					if(zahlZwei == 0.0) {
						System.out.print("Der Divisor ist 0. Ung�ltige mathematische Operation.");
					}
					else {
						ergebnis = zahlEins / zahlZwei;
						System.out.print("Die Summe der Summanden " + zahlEins + ", und " + zahlZwei + " ist: " + ergebnis);
						operatorPruefung = false;
					}
					break;
				}
			}
			else {
				System.out.println("Sie haben ein unzul�ssiges Zeichen eingegeben.\n");
			}
		}
	}
}
