/*
AB "Schleifen 2": Aufgabe 6 ("Million")
Entwickeln Sie ein Programm, welches die Anzahl der Jahre berechnet, bis Sie aufgrund
einer einmaligen Einlage und einem konstanten Zinssatz Million�r geworden sind. Die H�he
der Einlage und der Zinssatz sollen vom Benutzer eingegeben werden. Die Anzahl der Jahre
(auf ganze Jahre aufgerundet) soll vom Programm ausgegeben werden.
Nach der Ausgabe soll der Benutzer gefragt werden, ob er einen weiteren
Programmdurchlauf mit anderen Werten durchf�hren m�chte. Best�tigt er mit �j�, wird eine
weitere Rechnung durchgef�hrt, eine Eingabe von �n� beendet das Programm.
*/

import java.util.Scanner;

public class schleifen2A6 {
	
	public static void main(String[] args) {
		//VARIABLENDEKLARATION
		boolean endlos = true;
		boolean pruefungEins = true;
		boolean pruefungZwei = true;
		boolean pruefungDrei = true;
		double einlagenHoehe = 0.0;
		double zinssatz = 0.0;
		int zaehler = 1;
		double temp = 0.0;
		double grenzkapital = 1000000.0;
		double endkapital = 0.0;
		char nutzerEntscheidung = 'a';

		//PROGRAMMABLAUF: Berechnung und Nutzerabfrage
		while(endlos) {
			while(pruefungEins) {
				Scanner nutzerEingabe = new Scanner(System.in);
				System.out.print("Bitte die H�he Ihrer Einlage eingeben: ");
				einlagenHoehe = nutzerEingabe.nextDouble();
				System.out.print("Bitte die H�he Ihres Zinssatzes eingeben: ");
				zinssatz = nutzerEingabe.nextDouble();
				if(einlagenHoehe > 0) {
					pruefungEins = false;
				}
				else {
					System.out.println("Sie haben eine falsche Eingabe vorgenommen");
				}
			}
			
			while(pruefungZwei) {
				temp = einlagenHoehe + (1*(zinssatz/100));
				endkapital = endkapital + temp;
				if(endkapital > grenzkapital) {
					pruefungZwei = false;
				}
				else {
					zaehler += 1;
				}
			}
			
			System.out.println("Bei einer Einlagenh�he von " + einlagenHoehe + " und einem Zinssatz von " + zinssatz + " w�rden Sie " + zaehler + " Jahre ben�tigen um 1.000.000 EUR zu sparen.");
			
			while(pruefungDrei) {
				System.out.print("Wollen Sie die Bearbeitung fortsetzen? (j/n): ");
				Scanner entscheidungScanner = new Scanner(System.in);
				nutzerEntscheidung = entscheidungScanner.next().charAt(0);
				if(nutzerEntscheidung == 'j') {
					System.out.println("Das Programm wird erneut ausgef�hrt.");			
					pruefungDrei = false;
				}
				else if(nutzerEntscheidung == 'n') {
					System.out.println("Das Programm wird beendet.");
					endlos = false;
					pruefungDrei = false;
				}
				else {
					System.out.println("Sie haben eine falsche Eingabe get�tigt.");
				}	
			}
		}	
	}	
}
