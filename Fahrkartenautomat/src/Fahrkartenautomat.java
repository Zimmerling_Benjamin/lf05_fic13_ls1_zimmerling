import java.util.Scanner;

class Fahrkartenautomat
{
	
	public static double fahrkartenbestellungErfassen() {
	   System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
	   System.out.println("(1) Einzelfahrschein Regeltarif AB [2.90 EUR]");
	   System.out.println("(2) Tageskarte Regeltarif AB [8,60 EUR]");
	   System.out.println("(3) Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]");
	   Scanner eingabe = new Scanner(System.in);
	   int auswahl = eingabe.nextInt();
	   double zuZahlenderBetrag = 0.0;
	   byte anzahlTickets = 0;
	   switch(auswahl) {
	   case 1:
		   System.out.println("Zu zahlender Betrag (EURO): 2,90");
	       zuZahlenderBetrag = 2.90;
	       System.out.print("\nBitte geben Sie die Anzahl ben�tigter Tickets ein: ");
	       anzahlTickets = eingabe.nextByte();
	       zuZahlenderBetrag = anzahlTickets * zuZahlenderBetrag;
		   break;
	   case 2:
		   System.out.println("Zu zahlender Betrag (EURO): 8,60");
	       zuZahlenderBetrag = 8.60;
	       System.out.print("\nBitte geben Sie die Anzahl ben�tigter Tickets ein: ");
	       anzahlTickets = eingabe.nextByte();
	       zuZahlenderBetrag = anzahlTickets * zuZahlenderBetrag;
		   break;
	   case 3:
		   System.out.println("Zu zahlender Betrag (EURO): 23,50");
	       zuZahlenderBetrag = 23.50;
	       System.out.print("\nBitte geben Sie die Anzahl ben�tigter Tickets ein: ");
	       anzahlTickets = eingabe.nextByte();
	       zuZahlenderBetrag = anzahlTickets * zuZahlenderBetrag;
		   break;
	   default:
		   System.out.println("Es wurde eine falsche Auswahl getroffen.");
	   }
       return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
	    Scanner eingabe = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0;
	    double eingeworfeneM�nze = 0;
	    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	    {
	    	System.out.printf("Noch zu zahlen: %.2f",+(zuZahlenderBetrag- eingezahlterGesamtbetrag));
	    	System.out.print(" EURO.\n");
	    	System.out.print("\nEingabe (mind. 0,05 EURO, h�chstens 2,00 EURO): ");
	    	eingeworfeneM�nze = eingabe.nextFloat();
	        eingezahlterGesamtbetrag += eingeworfeneM�nze;
	    }	
	    return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	    for (int i = 0; i < 8; i++)
	    {
	    	System.out.print("=");
	    	try {
	    		Thread.sleep(250);
	    	} catch (InterruptedException e) {
	    		// TODO Auto-generated catch block
	    		e.printStackTrace();
	    	}
	    }
	    System.out.println("\n\n");
	}
	
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
	       double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f",+ r�ckgabebetrag);
	    	   System.out.print(" Euro");
	    	   System.out.println(" wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2,00 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1,00 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("0,50 EURO");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("0,20 EURO");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("0,10 EURO");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("0,05 EURO");
	 	          r�ckgabebetrag -= 0.05;
	           }
	           while(r�ckgabebetrag >= 0.001)// 5 CENT-M�nzen
	           {
	        	  System.out.println("0,05 EURO");
	 	          r�ckgabebetrag -= 0.01;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n");		
	}
	
	public static void main(String[] args)
    {            
		boolean endlos = true;
		while(endlos) {
			boolean pruefung = true;
	    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
	    	double eingezahlterBetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	    	fahrkartenAusgeben();
	    	rueckgeldAusgeben(eingezahlterBetrag, zuZahlenderBetrag);
	    	while(pruefung) {
		    	System.out.println();
		    	Scanner nutzerEingabe = new Scanner(System.in);
		    	System.out.print("M�chten Sie einen weiteren Kauf t�tigen? (J/N): ");
		    	char nutzerAuswahl = nutzerEingabe.next().charAt(0);
		    	nutzerAuswahl = java.lang.Character.toUpperCase(nutzerAuswahl);
		    	if(nutzerAuswahl == 'N') {
		    		System.out.println();
		    		System.out.println("Wir w�nschen Ihnen eine gute Fahrt.");
		    		System.out.println();
		    		endlos = false;
		    		pruefung = false;
		    		nutzerEingabe.close();
		    	}
		    	else if(nutzerAuswahl == 'J') {
		    		System.out.println();
		    		System.out.println("Kaufvorgang wird neu gestartet.");
		    		System.out.println();
		    		pruefung = false;
		    	}
		    	else {
		    		System.out.println();
		    		System.out.println("Es wurde eine falsche Eingabe get�tigt.");
		    		System.out.println();
		    	}	
	    	}
		}
    }
}