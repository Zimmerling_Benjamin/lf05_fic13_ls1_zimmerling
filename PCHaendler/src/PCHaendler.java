import java.util.Scanner;
public class PCHaendler {

	//Eingabemethode: Zeichenkette (String) durch den Nutzer einzulesen
	public static String liesString() {						//Methodensignatur
		Scanner scannerString = new Scanner(System.in);		//Scannerobjekt erstellen und �ffnen
		String text = scannerString.next();					//Scannerobjekt soll String vom Nutzer einlesen
		//scannerString.close();								//Scannerobjekt schlie�en und "vernichten"
		return text; 										//Gebe den Wert in der Variable text zur�ck
	}

	//Eingabemethode: Ganzzahl (Integer) durch den Nutzer einzulesen
	public static int liesInt() {							//Methodensignatur
		Scanner scannerInt = new Scanner(System.in);		//Scannerobjekt erstellen und �ffnen
		int ganzeZahl = scannerInt.nextInt();				//Scannerobjekt soll Integer vom Nutzer einlesen
		//scannerInt.close();									//Scannerobjekt schlie�en und "vernichten"
		return ganzeZahl; 									//Gebe den Wert in der Variable ganzeZahl zur�ck
	}
	
	//Eingabemethode: Gleitkommazahl (Double) durch Nutzer einzulesen
	public static double liesDouble() {							//Methodensignatur
		Scanner scannerDouble = new Scanner(System.in);			//Scannerobjekt erstellen und �ffnen
		double gleitkommaZahl = scannerDouble.nextDouble();		//Scannerobjekt soll Double vom Nutzer einlesen
		//scannerDouble.close();									//Scannerobjekt schlie�en und "vernichten"
		return gleitkommaZahl; 									//Gebe den Wert in der Variable gleitkommaZahl zur�ck
	}
	
	//Verarbeitungsmethode: Berechnung des Nettogesamtpreises
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double gesamtnettopreis = anzahl * preis;
		return gesamtnettopreis;
	}
	
	//Verarbeitungsmethode: Berechnung des Nettogesamtpreises
	public static double berechneGesamtbruttopreis(double gesamtnettopreis, double mwst) {
		double gesamtbruttopreis = gesamtnettopreis * (1 + mwst / 100);
		return gesamtbruttopreis;
	}
	
	//Ausgabemethode: Ausgabe des Rechnungsbelegs
	public static void rechnungausgeben(String artikel, int anzahl, double gesamtnettopreis, double gesamtbruttopreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, gesamtnettopreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, gesamtbruttopreis, mwst, "%");
	}

	//Main-Methode: 
	public static void main(String[] args) {
		//Benutzereingaben lesen --> E vom EVA-Prinzip (Eingabe)
		System.out.println("was m�chten Sie bestellen?");
		String artikel = liesString();							//Methodenaufruf liesString
		System.out.println();
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt();									//Methodenaufruf liesInt
		System.out.println();
		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble();							//Methodenaufruf liesDouble
		System.out.println();
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesDouble();								//Methodenaufruf liesDouble

		// Verarbeiten --> V vom EVA-Prinzip (Verarbeitung)
		double gesamtnettopreis = berechneGesamtnettopreis(anzahl, preis); 			  //Methodenaufruf: berechneGesamtnettopreis
		double gesamtbruttopreis = berechneGesamtbruttopreis(gesamtnettopreis, mwst); //Methodenaufruf: berechneGesamtbruttopreis

		// Ausgeben --> A vom EVA-Prinzip (Ausgabe)
		rechnungausgeben(artikel, anzahl, gesamtnettopreis, gesamtbruttopreis, mwst); //Methodenaufruf: rechnungausgeben
	}

}