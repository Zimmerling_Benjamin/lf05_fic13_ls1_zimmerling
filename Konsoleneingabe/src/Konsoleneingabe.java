import java.util.Scanner;

public class Konsoleneingabe {
	
	public static void main(String[] args) {
	
		//Beispiel
		System.out.println("***Beispielaufgabe - Tafel***");
		System.out.println();
		Scanner eingabe = new Scanner(System.in);
		System.out.println("Bitte eine ganze Zahl eingeben: ");
		int zahlenEingabe = eingabe.nextInt();
		System.out.println();
		System.out.println("Ihre eingegebene Zahl ist: "+zahlenEingabe);
		//ScannerObjekt schließen
		//eingabe.close();

		System.out.println();
		System.out.println("***Aufgabe 1 - Übungsblatt***");
		System.out.println();
		//Neues Scanner-Objekt myScanner wird erstellt
		System.out.println("Bitte geben Sie eine ganze Zahl ein: ");
		// Die Variable zahl1 speichert die erste Eingabe
		int zahl1 = eingabe.nextInt();
		System.out.println("Bitte geben Sie eine zweite ganze Zahl ein: ");
		// Die Variable zahl2 speichert die zweite Eingabe
		int zahl2 = eingabe.nextInt();
		// Die Addition der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		int ergebnisAddition = zahl1 + zahl2;
		int ergebnisSubtraktion = zahl1 - zahl2;
		int ergebnisMultiplikation = zahl1 * zahl2;
		int ergebnisDivision = zahl1 / zahl2;
		double kommazahl1 = zahl1;
		double kommazahl2 = zahl2;
		double kommaErgebnisDivision = kommazahl1 / kommazahl2;
		System.out.println();
		System.out.print("Ergebnis der Addition lautet: ");
		System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnisAddition);
		System.out.print("Ergebnis der Subtraktion lautet: ");
		System.out.println(zahl1 + " - " + zahl2 + " = " + ergebnisSubtraktion);
		System.out.print("Ergebnis der Multiplikation lautet: ");
		System.out.println(zahl1 + " * " + zahl2 + " = " + ergebnisMultiplikation);
		System.out.print("Ergebnis der ganzzahligen Division lautet: ");
		System.out.println(zahl1 + " / " + zahl2 + " = " + ergebnisDivision);
		System.out.print("Ergebnis der Division lautet: ");
		System.out.println(kommazahl1 + " / " + kommazahl2 + " = " + kommaErgebnisDivision);
		//ScannerObjekt schließen
		//eingabe.close();
		 
		//Aufgabe 2: Name und Alter abfragen
		System.out.println();
		System.out.println("***Aufgabe 2 - Übungsblatt***");
		System.out.println();
		System.out.println("Bitte Ihren Namen eingeben: ");
		String name = eingabe.next();
		System.out.println("Bitte Ihr Alter eingaben: ");
		byte alter = eingabe.nextByte();
		eingabe.close();
		System.out.println();		
		System.out.println("Hallo "+name+".");
		System.out.println("Sie sind aktuell "+alter+" Jahre alt.");
	}
}
