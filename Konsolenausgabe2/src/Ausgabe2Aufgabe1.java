public class Ausgabe2Aufgabe1 {

	public static void main(String[] args) {
	
		//Teilaufgabe 1: Ausgabe eines Sternquadrats
		System.out.printf("%12s", "**\n");
		System.out.printf("%6s", "*");
		System.out.printf("%10s", "*\n");
		System.out.printf("%6s", "*");
		System.out.printf("%10s", "*\n");
		System.out.printf("%12s", "**\n");
	}
	
}
