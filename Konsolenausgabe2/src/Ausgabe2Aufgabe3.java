public class Ausgabe2Aufgabe3 {

	public static void main(String[] args) {
		
		//Teilaufgabe 3:
		
		//Variablen deklarieren
		double zahlEins = -28.8889;
		double zahlZwei = -23.3333;
		double zahlDrei = -17.7778;
		double zahlVier = -6.6667;
		double zahlFuenf = -1.1111;
		
		//Tabellenausgabe
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%11s", "Celsius\n");
		System.out.println("-----------------------");
		System.out.printf("%-12s", "-20");
		System.out.print("|");
		System.out.printf("%3s %.2f\n", " ", zahlEins);
		System.out.printf("%-12s", "-10");
		System.out.print("|");
		System.out.printf("%3s %.2f\n", " ", zahlZwei);
		System.out.printf("%-12s", "+0");
		System.out.print("|");
		System.out.printf("%3s %.2f\n", " ", zahlDrei);
		System.out.printf("%-12s", "+10");
		System.out.print("|");
		System.out.printf("%4s %.2f\n", " ", zahlVier);
		System.out.printf("%-12s", "+20");
		System.out.print("|");
		System.out.printf("%4s %.2f\n", " ", zahlFuenf);
		
	}
}
