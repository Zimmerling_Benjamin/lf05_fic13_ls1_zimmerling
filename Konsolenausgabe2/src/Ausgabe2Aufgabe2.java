public class Ausgabe2Aufgabe2 {

	public static void main(String[] args) {
		
		//Teilaufgabe 2: Ausgabe der Fakult�t

		//Variablendeklaration
		int zahlEins = 1;
		int zahlZwei = 1*2;
		int zahlDrei = 1*2*3;
		int zahlVier = 1*2*3*4;
		int zahlFuenf = 1*2*3*4*5;
		
		//Fakult�t Ausgabe
		System.out.printf("%2s", "0!");
		System.out.printf("%4s", "=");
		System.out.printf("%20s", "=");
		System.out.printf("%4d\n", zahlEins);
		
		System.out.printf("%2s", "1!");
		System.out.printf("%4s", "=");
		System.out.printf("%2s", "1");
		System.out.printf("%18s", "=");
		System.out.printf("%4d\n", zahlEins);
		
		System.out.printf("%2s", "2!");
		System.out.printf("%4s", "=");
		System.out.printf("%6s", "1 * 2");
		System.out.printf("%14s", "=");
		System.out.printf("%4d\n", zahlZwei);
		
		System.out.printf("%2s", "3!");
		System.out.printf("%4s", "=");
		System.out.printf("%10s", "1 * 2 * 3");		
		System.out.printf("%10s", "=");
		System.out.printf("%4d\n", zahlDrei);
		
		System.out.printf("%2s", "4!");
		System.out.printf("%4s", "=");
		System.out.printf("%14s", "1 * 2 * 3 * 4");		
		System.out.printf("%6s", "=");
		System.out.printf("%4d\n", zahlVier);
		
		System.out.printf("%2s", "5!");
		System.out.printf("%4s", "=");
		System.out.printf("%18s", "1 * 2 * 3 * 4 * 5");		
		System.out.printf("%2s", "=");
		System.out.printf("%4d\n", zahlFuenf);
	}
}
