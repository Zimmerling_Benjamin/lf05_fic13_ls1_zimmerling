
public class Ausgabe1Aufgabe3 {

	public static void main(String[] args) {
		
		//Variablendeklaration
		double zahlEins = 22.4234234;
		double zahlZwei = 111.2222;
		double zahlDrei = 4.0;
		double zahlVier = 1000000.551;
		double zahlFuenf = 97.34;
	
		//Ausgabe von Zahlen
		System.out.printf("%.2f\n", zahlEins);
		System.out.printf("%.2f\n", zahlZwei);
		System.out.printf("%.2f\n", zahlDrei);
		System.out.printf("%.2f\n", zahlVier);
		System.out.printf("%.2f\n", zahlFuenf);

	}
	
}
