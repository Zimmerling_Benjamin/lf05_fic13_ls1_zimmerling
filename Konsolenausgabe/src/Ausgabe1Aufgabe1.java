
public class Ausgabe1Aufgabe1 {

	public static void main(String[] args) {
		
		//Teilaufgabe 1: 2 erdachte Beispiels�tze mit 2 Print-Befehlen
		System.out.print("Das Wetter ist zu sch�n um programmieren mit Java zu lernen.");
		System.out.println("Python w�re da angenehmer.");
		
		//Breakline
		System.out.println ("");
		System.out.println ("------------------------");
		System.out.println ("");
		
		//Teilaufgabe 2: Escape-Zeichen und String-Verkettung
		System.out.print("Das Wetter ist zu sch�n um programmieren mit der Sprache \"Java\" zu lernen.\n"
		+ "Die Programmiersprache \"Python\" w�re da angenehmer.");
		
		//Breakline
		System.out.println ("");
		System.out.println ("");
		System.out.println ("------------------------");
		System.out.println ("");
		
		//Teilaufgabe 3: Kommentare schreiben
		
		/*
		 * Zwei Slashes (//) erzeugen einen einzeiligen Kommentar
		 * Die Kombination aus Slash und Stern �ffnet und schlie�t einen mehrzeiligen Kommentar
		 * 
		 * Unterschied zwischen den Methoden print() und println()
		 * Methode print(): Erzeugt die Ausgabe einer Zeichenkette (String) ohne Zeilenumbruch
		 * Methode println(): Erzeugt die Ausgabe einer Zeichenkette (String) mit Zeilenumbruch
		 */

	}

}


